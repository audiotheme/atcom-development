# Development Settings

Persists development-related settings when pulling the database using WP Migrate DB Pro.

This shouldn't be installed or activated on production.
