<?php
/**
 * Plugin Name: ATCOM Development Settings
 * Description: Development settings.
 * Version: 1.0.1
 * Author: AudioTheme
 * Author URI: https://audiotheme.com/
 * License: GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @link http://snippets.webaware.com.au/snippets/use-wp-migrate-db-pro-and-keep-your-development-environment-settings/
 * @link https://github.com/deliciousbrains/wp-migrate-db-pro-tweaks/blob/master/wp-migrate-db-pro-tweaks.php
 */

/**
 *
 */
add_action( 'wpmdb_migration_complete', function( $type, $location ) {
	if ( 'pull' !== $type ) {
		return;
	}

	// Force options refresh.
	wp_cache_flush();

	// WooCommerce email settings.
	$settings = get_option('woocommerce_new_order_settings');
	if ( is_array( $settings ) ) {
		$settings['recipient'] = '';
		update_option( 'woocommerce_new_order_settings', $settings );
	}

	// WooCommerce PayPal settings.
	$settings = get_option( 'woocommerce_paypal_settings' );
	if ( is_array( $settings ) ) {
		$settings['testmode'] = 'yes';
		update_option( 'woocommerce_paypal_settings', $settings );
	}

	// WooCommerce Stripe settings.
	$settings = get_option( 'woocommerce_stripe_settings' );
	if ( is_array( $settings ) ) {
		$settings['testmode'] = 'yes';
		update_option( 'woocommerce_stripe_settings', $settings );
	}
}, 10, 2 );

/**
 *
 */
add_filter( 'wpmdb_preserved_options', function( $options ) {
	$options = array(
		'active_plugins',
		'blog_public',
		'blogname',
		'wpmdb_settings',
		'wpsatellite_active_plugins',
	);

	return array_unique( $options );
} );

/**
 * 
 */
add_filter( 'http_request_host_is_external', function( $allow, $host ) {
	if ( 'local.wordpress.dev' === $host ) {
		return true;
	}

	return $allow;
}, 10, 2 );
